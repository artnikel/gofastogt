package gofastogt

import (
	"encoding/json"
	"os"
)

type IBytesConverter interface {
	ToBytes() (json.RawMessage, error)
}

func IsRunningInDocker() bool {
	_, exists := os.LookupEnv("DOCKER_CONTAINER_ID")
	return exists
}
