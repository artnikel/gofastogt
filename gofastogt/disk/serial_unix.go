//go:build unix

package disk

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/sys/unix"
)

// GetEnv retrieves the environment variable key. If it does not exist it returns the default.
func getEnv(key string, dfault string, combineWith ...string) string {
	value := os.Getenv(key)
	if value == "" {
		value = dfault
	}

	switch len(combineWith) {
	case 0:
		return value
	case 1:
		return filepath.Join(value, combineWith[0])
	default:
		all := make([]string, len(combineWith)+1)
		all[0] = value
		copy(all[1:], combineWith)
		return filepath.Join(all...)
	}
}

func hostRun(combineWith ...string) string {
	return getEnv("HOST_RUN", "/run", combineWith...)
}

func GetDiskSerialNumber(name string) (*string, error) {
	var stat unix.Stat_t
	err := unix.Stat(name, &stat)
	if err != nil {
		return nil, err
	}

	major := unix.Major(uint64(stat.Dev))
	minor := unix.Minor(uint64(stat.Dev))

	udevDataPath := hostRun(fmt.Sprintf("udev/data/b%d:%d", major, minor))
	if udevdata, err := ioutil.ReadFile(udevDataPath); err == nil {
		scanner := bufio.NewScanner(bytes.NewReader(udevdata))
		var fsuud *string
		for scanner.Scan() {
			values := strings.Split(scanner.Text(), "=")
			if len(values) == 2 {
				if values[0] == "E:ID_SERIAL_SHORT" {
					return &values[1], nil
				} else if values[0] == "E:ID_FS_UUID" {
					fsuud = &values[1]
				}
			}
		}
		if fsuud != nil {
			return fsuud, nil
		}
	}

	return nil, errors.New("not found")
}
