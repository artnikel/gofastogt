package gofastogt

import (
	"context"
	"sync"
)

type executionFn func(ctx context.Context, args any) (any, error)

type Wjob struct {
	ExecFn executionFn
	Args   any
}

func (j *Wjob) Execute(ctx context.Context) Result {
	result, err := j.ExecFn(ctx, j.Args)
	if err != nil {
		return Result{Err: err}
	}
	return Result{Value: result}
}

type Result struct {
	Value any
	Err   error
}

// WorkerPool struct
type WorkerPool struct {
	workersCount int
	jobs         chan Wjob
	resutls      chan Result
}

// workersCount must be more than 0
func NewWorkerPool(workersCount int) WorkerPool {
	return WorkerPool{workersCount: workersCount,
		jobs:    make(chan Wjob, workersCount),
		resutls: make(chan Result, workersCount),
	}
}

// Function for running one Worker.
func workerRun(ctx context.Context, jobs <-chan Wjob, results chan<- Result) {
	for {
		select {
		// Worker read jobs from chan. If channel is close worker finish his job
		case job, ok := <-jobs:
			if !ok {
				return
			}
			// Execute job and write result
			results <- job.Execute(ctx)
		case <-ctx.Done():
			// Canceling work and return error in result
			results <- Result{Err: ctx.Err()}
			return
		}
	}
}

func (wp *WorkerPool) Run(ctx context.Context) {
	var wg sync.WaitGroup
	for i := 0; i < wp.workersCount; i++ {
		wg.Add(1)
		// Start workers
		// Workers read job from channel and write result into wp.result.
		// Workers work until jobs channel will not be closed and empty or execute ctx.Done()
		go func() {
			workerRun(ctx, wp.jobs, wp.resutls)
			wg.Done()
		}()
	}
	wg.Wait()
	// Close channel when work is finished
	close(wp.resutls)
}

// Func return chan with results
func (wp *WorkerPool) Result() <-chan Result {
	return wp.resutls
}

// Add list of Jobs.
// After exequtive func, jobs channel will be closed
func (wp *WorkerPool) AddJobs(job ...Wjob) {
	for i := range job {
		wp.jobs <- job[i]
	}
	close(wp.jobs)
}
