package unittests

import (
	"testing"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestIsRunningInDocker(t *testing.T) {
	if !gofastogt.IsRunningInDocker() {
		t.Error("Expected to return true when DOCKER_CONTAINER_ID is set, but got false")
	}
}
