package unittests

import (
	"context"
	"errors"
	"reflect"
	"testing"
	"time"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

const (
	jobsCount   = 10
	workerCount = 5
)

var (
	errDefault = errors.New("wrong argument type")

	// test func return result argVal*2
	execFn = func(ctx context.Context, args any) (any, error) {
		argVal, ok := args.(int)
		if !ok {
			return nil, errDefault
		}
		//sleep for testing context.Done()
		time.Sleep(time.Millisecond * 50)
		return argVal * 2, nil
	}
)

func TestJob_execute(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name string
		j    *gofastogt.Wjob
		args args
		want gofastogt.Result
	}{
		{
			name: "test Execute with error",
			j: &gofastogt.Wjob{
				ExecFn: execFn,
				Args:   "2",
			},
			args: args{ctx: context.TODO()},
			want: gofastogt.Result{
				Err: errDefault,
			},
		},
		{
			name: "test Execute no error",
			j: &gofastogt.Wjob{
				ExecFn: execFn,
				Args:   jobsCount,
			},
			args: args{ctx: context.TODO()},
			want: gofastogt.Result{
				Err:   nil,
				Value: jobsCount * 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.j.Execute(tt.args.ctx)
			if got.Err != nil {
				if !reflect.DeepEqual(got.Err.Error(), tt.want.Err.Error()) {
					t.Errorf("Job.execute() error = %v, want error %v", got, tt.want)
				}
				return
			}
			if !reflect.DeepEqual(got.Value.(int), tt.want.Value.(int)) {
				t.Errorf("Job.execute() value = %v, want value %v", got, tt.want)
			}
		})
	}
}

func TestWorkerPool(t *testing.T) {
	wp := gofastogt.NewWorkerPool(workerCount)
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	go wp.Run(ctx)
	go wp.AddJobs(testJobs()...)
	for r := range wp.Result() {
		_, ok := r.Value.(int)
		if !ok {
			t.Fatal("Test workerPool. Wrong type of value in result")
		}
	}
}

func TestWorkerPool_Cancel(t *testing.T) {
	wp := gofastogt.NewWorkerPool(workerCount)
	ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Millisecond)
	defer cancel()
	jobs := testJobs()
	go wp.Run(ctx)
	go wp.AddJobs(jobs...)
	for r := range wp.Result() {
		if r.Err != nil && r.Err != context.DeadlineExceeded {
			t.Fatalf("Expected error:%v; got: %v", context.DeadlineExceeded, r.Err)
		}
	}
}

func testJobs() []gofastogt.Wjob {
	jobs := make([]gofastogt.Wjob, jobsCount)
	for i := 0; i < jobsCount; i++ {
		jobs[i] = gofastogt.Wjob{
			ExecFn: execFn,
			Args:   jobsCount,
		}
	}
	return jobs
}
