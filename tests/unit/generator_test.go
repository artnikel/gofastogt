package unittests

import (
	"reflect"
	"testing"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestGenerate(t *testing.T) {
	tests := []struct {
		name    string
		count   int
		want    int
		wantErr bool
	}{
		{name: "case_1",
			count:   8,
			want:    8,
			wantErr: false,
		},
		{name: "case_2",
			count:   3,
			want:    3,
			wantErr: false,
		},
		{name: "case_3",
			count:   0,
			want:    0,
			wantErr: true,
		},
		{name: "case_4",
			count:   -1,
			want:    0,
			wantErr: true,
		},
		{name: "case_5",
			count:   16,
			want:    16,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.GenerateString(tt.count)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !(got == nil && tt.wantErr) && !reflect.DeepEqual(len(*got), tt.want) {
				t.Errorf("%v", err)
				t.Errorf("len(GenerateString()) = %v, want %v", len(*got), tt.want)
			}
		})
	}
}
